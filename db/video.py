# create class video that contains all video information (uuid, name,path)
# path: db\video.py

import mariadb
import sys
import db.db_manager as db

class Video:
    def __init__(self, uuid, name, path, date):
        self.uuid = uuid
        self.name = name
        self.path = path
        self.date = date

    def __str__(self):
        return f"uuid: {self.uuid}, name: {self.name}, path: {self.path}"

    def get_uuid(self):
        return self.uuid

    def get_name(self):
        return self.name

    def get_path(self):
        return self.path

    def set_uuid(self, uuid):
        self.uuid = uuid

    def set_name(self, name):
        self.name = name

    def set_path(self, path):
        self.path = path
