# connect to mariadb
import mariadb
import sys


def connect():
    try:
        conn = mariadb.connect(
            user="adminInfra",
            password="test",
            host="51.77.137.116",
            port=1733,
            database="projet_infra"
        )
        cur = conn.cursor()
    except mariadb.Error as e:
        print(f"Error connecting to MariaDB Platform: {e}")
        sys.exit(1)


if __name__ == "__main__":
    connect()
