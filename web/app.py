from flask import Flask, render_template, request, redirect, url_for, flash, Response
import functions.db_manager as db

app = Flask(__name__)

# ---------------- Routes ----------------
app = Flask(__name__)
@app.route('/')
def index():
    return render_template('index.html')

@app.route('/video_feed')
def video_feed():
    return Response(gen_frames(), mimetype='multipart/x-mixed-replace; boundary=frame')

@app.route('/login', methods=['GET','POST'])
def login():  
    if request.method == 'POST':
        username = request.form['username']
        passwd = request.form['passwd']
        if db.check_login(username, passwd):
            return redirect(url_for('index'))
        else:
            return render_template('login.html')
    return render_template('login.html')



@app.route('/register', methods=['GET', 'POST'])
def register():
    #if request.method == 'POST':
    #     title = request.form['title']
    #     content = request.form['content']

    #     if not title:
    #         flash('Title is required!')
    #     elif not content:
    #         flash('Content is required!')
    #     else:
    #         messages.append({'title': title, 'content': content})
    #         return redirect(url_for('index'))
    #     return redirect(url_for('login'))
    return render_template('register.html')

