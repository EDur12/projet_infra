# Module Imports
import mariadb
import sys
import bcrypt



#---------------- Functions ----------------#

def connect_to_db():
    try:
        conn = mariadb.connect(
            user="adminInfra",
            password="test",
            host="51.77.137.116",
            port=3306,
            database="projet_infra"
        )
        print ("Connected to MariaDB Platform\n")
        cur = conn.cursor()
        return cur, conn
    except mariadb.Error as e:
        print(f"Error connecting to MariaDB Platform: {e}")
        sys.exit(1)
    
cur, conn = connect_to_db()

def create_database():
    try:
        cur.execute("CREATE DATABASE IF NOT EXISTS projet_infra")
        print("Database created\n")
    except mariadb.Error as e:
        print(f"Error: {e}")

def create_user_table():
    try:
        cur.execute("CREATE TABLE IF NOT EXISTS user (id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, first_name VARCHAR(50) NOT NULL, last_name VARCHAR(50) NOT NULL, email VARCHAR(50) NOT NULL, password VARCHAR(255) NOT NULL)")
        print("Table created\n")
    except mariadb.Error as e:
        print(f"Error: {e}")



def add_user(first_name, last_name, email, password):
    try:
        cur.execute("INSERT INTO user (first_name, last_name, email, password) VALUES (?, ?, ?, ?)", (first_name, last_name, email, password))
        conn.commit()
        print("User added\n")
    except mariadb.Error as e:
        print(f"Error: {e}")



def get_user_by_email(email):
    cur.execute("SELECT * FROM user WHERE email = ?", (email,))
    return cur.fetchone()

def get_user_by_id(id):
    cur.execute("SELECT * FROM user WHERE id = ?", (id,))
    return cur.fetchone()

def get_all_users():
    cur.execute("SELECT * FROM user")
    print ("--------- All users ---------")
    for user in cur:
        print(user)
    print("\n")


def update_user(id, first_name, last_name, email, password):
    cur.execute("UPDATE user SET first_name = ?, last_name = ?, email = ?, password = ? WHERE id = ?", (first_name, last_name, email, password, id))
    conn.commit()

def delete_user(id):
    cur.execute("DELETE FROM user WHERE id = ?", (id,))
    conn.commit()

def get_user_by_name(first_name, last_name):
    cur.execute("SELECT * FROM user WHERE first_name = ? AND last_name = ?", (first_name, last_name))
    return cur.fetchone()

def get_password_by_email(email):
    cur.execute("SELECT password FROM user WHERE email = ?", (email,))
    return cur.fetchone()

def check_login(email, password):
    if get_user_by_email(email) is not None:
        if get_password_by_email(email)[0] == password:
            print("Logged in !")
        else:
            print("Wrong password !")

def check_register(first_name, last_name, email, password):
    if get_user_by_email(email) is None:
        add_user(first_name, last_name, email, hash_password(password.encode('utf-8')))
        print("Registered !")
    else:
        print("Email already used !")

def hash_password(password):
    # Adding the salt to password
    salt = bcrypt.gensalt()
    # Hashing the password
    password = bcrypt.hashpw(password, salt)
    return password


#---------------- Test ----------------#
# create_database()
# create_user_table()
# get_all_users()
print("lol")



