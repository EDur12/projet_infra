print("Loading db_structure.py")
class User(object):
    def __init__(self,first_name, last_name, email, password):
        self.first_name = first_name
        self.last_name = last_name
        self.email = email
        self.password = password
    
    def get_first_name(self):
        return self.first_name
    
    def get_last_name(self):
        return self.last_name

    def get_mail(self):
        return self.email

    def get_password(self):
        return self.password
    
    def set_first_name(self,first_name):
        self.first_name = first_name
    
    def set_last_name(self,last_name):
        self.last_name = last_name
    
    def set_mail(self,email):
        self.email = email
    
    def set_password(self,password):
        self.password = password
    
    
