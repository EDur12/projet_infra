# VisionAId Project
VisionAId is a prototype of a mobile app for help blind people to navigate in the world.
It stream video from camera and detect objects and places in real time. It also uses Core ML to classify images using a pre-trained model.
The stream is sent to a server and the server send the stream to a website.

## Architecture

![Architecture](./pics/shema_infra.drawio.png)

## App

[- App](./mobile)

## MariaDB

[- MariaDB](./db)

## Website

[- Website](./web)