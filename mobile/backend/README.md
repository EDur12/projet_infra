# VisonAId Mobile Backend
This is the backend for the VisonAId mobile app. It is a tcp server managed by a systemd service. It is written in python3 and manage user identification, token generation, and nginx-rtmp container management.

## Requirements
- Python3
- Docker
- Docker-compose
- Nginx-rtmp-module docker image
- ffmpeg (for quick testing)
- systemd

### systemd file:
```
[Unit]
Description=VisionAid Mobile Backend
After=network.target

[Service]
Type=simple
User=visionaid
Group=visionaid
WorkingDirectory=/home/visionaid/B2/info/projet_infra/mobile/backend/src
ExecStart=/usr/bin/python3 ./main.py
Restart=always
RestartSec=3

[Install]
WantedBy=multi-user.target
```

## Installation
- Clone the repo
- Install the [requirements.txt](./src/requirements.txt)
- Copy the service file to systemd

## Usage
- Start the service
- Connect to the server with the app
- Register on the website
- Start the stream
- access to the stream url from the website
