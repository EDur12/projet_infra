#!/bin/bash
# This script remove nginx RTMP server containers
# author: LexIt
# date: 2023-04-09

if [ $# -eq 0 ]
  then
    echo "Please provide a dockers directory path as an argument"
    exit 1
fi

# Define the port number as a variable
CONTAINER_PATH=$1

cd $CONTAINER_PATH

# Stop the docker-compose service
docker compose down --remove-orphans
# remove directory
cd ..
rm -r $CONTAINER_PATH

# Disallow the port in the firewall
ufw delete allow $CONTAINER_PATH

echo "Server on port $CONTAINER_PATH has been stopped"