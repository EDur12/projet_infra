import socket
from _thread import *
import os
import uuid

# Global Var:
ServerSideSocket = socket.socket()
host = "54.37.153.179"
port = 2005
ThreadCount = 0
message = ""
rtmp_port = 1936
userList = []

# Server:
try:
    ServerSideSocket.bind((host, port))
except socket.error as e:
    print(str(e))
print(f"TCP server is listening at {host}:{port}")
ServerSideSocket.listen(5)


# Main THREAD:
def multi_threaded_client(connection):
    global ThreadCount, rtmp_port
    # connection.send(str.encode(f"Server is working: {ThreadCount}"))
    while True:
        data = connection.recv(2048)
        print(f"data {data}")
        strData = data.decode("utf-8")
        print(f"data received: {strData}")

        # if data contain *userID*
        if strData.find("*connect*") == 0:
            user_id = strData.split('*connect*')[1]
            print(f"user {user_id} connect request received")
            # generate rtmp-container path
            path = f'../containers/rtmp-container{uuid.uuid4()}'
            userData = (user_id, path)
            userList.append(userData)
            # send back rtmp url
            response = f"rtmp://{host}:{rtmp_port}/live"
            print(f"userID: {response}")
            connection.send(str.encode(response))
            # run create_rtmp_stream.bash
            cmd = f"sudo bash /home/lexit/B2/info/projet_infra/mobile/backend/src/create-rtmp-server.sh {rtmp_port} {path}"
            os.system(cmd)
            # increment rtmp port
            rtmp_port += 1

        if strData.find("*disconnect*") == 0:
            user_id = strData.split('*disconnect*')[1]
            print(f"user {user_id} disconnect request received")
            # find user in userList
            for user in userList:
                if user[0] == user_id:
                    # stop rtmp stream
                    cmd = f"sudo bash /home/lexit/B2/info/projet_infra/mobile/backend/src/remove_rtmp_server.bash {user[1]}"
                    os.system(cmd)
                    # remove user from userList
                    userList.remove(user)
                    break
            response = "disconnected"
        if not data:
            break
    connection.sendall(str.encode(response))

    ThreadCount -= 1
    connection.close()


while True:
    Client, address = ServerSideSocket.accept()
    print("Connected to: " + address[0] + ":" + str(address[1]))
    start_new_thread(multi_threaded_client, (Client,))
    ThreadCount += 1
    print("Thread Number: " + str(ThreadCount))
