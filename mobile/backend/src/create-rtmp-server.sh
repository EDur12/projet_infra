#!/bin/bash
# This script creates a new nginx RTMP server in containers
# author: LexIt
# date: 2023-04-09

if [ $# -eq 0 ]
  then
    echo "Please provide a port number as an argument"
    exit 1
fi

# Define the port number as a variable
PORT=$1
WORKING_DIR=$2
TIME_STAMP=$(date +%Y-%m-%d_%H-%M-%S)

# Create the directory
echo "Created directory $WORKING_DIR"
mkdir $WORKING_DIR


# Change to the directory
cd $WORKING_DIR

# Create the nginx.conf file with the specified port
echo "worker_processes auto;
rtmp_auto_push on;
events {}
rtmp {
    server {
        listen $PORT;
        listen [::]:$PORT ipv6only=on;

        application live {
            live on;
            record off;
        }
    }
}" > nginx.conf

# Create the Dockerfile
echo "create Dockerfile"
echo "FROM tiangolo/nginx-rtmp

COPY nginx.conf /etc/nginx/nginx.conf" > Dockerfile

# Create the docker-compose.yml file
echo "version: '3'

services:
  rtmp-server:
    build:
      context: .
      dockerfile: Dockerfile
    ports:
      - \"$PORT:$PORT\"
    volumes:
      - ./nginx.conf:/etc/nginx/nginx.conf" > docker-compose.yml

echo "Files created successfully!"

# allow port
echo "Allowing port $PORT"
ufw allow $PORT

# Build the docker image
echo "Building docker image"
docker compose up -d
