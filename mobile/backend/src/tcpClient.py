import socket
host = "54.37.153.179"
port = 2005

def connect():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((host, port))
    return s

def send(s, data):
    s.sendall(str.encode(data))
    return s.recv(2048).decode("utf-8")

def main():
    s = connect()
    print(send(s, "Hello"))

if __name__ == "__main__":
    main()