# VisionAId IOS App
VisionAId stream video from camera and detect objects and places in real time. It also uses Core ML to classify images using a pre-trained model.
Is a prototype of a mobile app for help blind people to navigate in the world.

## Swift app
[- App](./app)

## Python/bash backend
[- backend](./backend)

## demo

### GoogleLeNetPlaces
GoogleLeNetPlaces is a deep learning architecture for scene recognition and image classification tasks. It is an extension of the Inception-v3 architecture, which was introduced by Google in 2015.

The GoogleLeNetPlaces architecture was specifically designed for recognizing places and scenes in images, such as landscapes, buildings, and indoor environments. It was trained on a large dataset called Places365, which contains over 1.8 million images categorized into 365 scene categories.

Compared to the original Inception-v3 architecture, GoogleLeNetPlaces has several modifications that make it more suitable for scene recognition. For example, it uses larger convolutional filters in the initial layers to capture more local spatial information, and it uses multiple parallel pathways with different filter sizes to capture both local and global features.

Overall, GoogleLeNetPlaces is a powerful architecture for scene recognition and has achieved state-of-the-art performance on several benchmark datasets, including Places365 and MIT Indoor Scene Recognition.
![GoogleLeNetPlaces](./pics/googleLeNetPlaces.PNG)
![GoogleLeNetPlaces_2](./pics/googlenetPlaces_2.PNG)
![GoogleLeNetPlaces_description](./pics/googleLeNetPlaces_description.PNG)

### SqueezeNet
SqueezeNet is a deep learning architecture for image classification tasks that was introduced by researchers at the University of California, Berkeley in 2016. It was designed to achieve state-of-the-art accuracy on image classification tasks while using a significantly smaller number of parameters than other popular architectures like VGG or ResNet.

The main idea behind SqueezeNet is to replace some of the larger filters typically used in convolutional layers with smaller 1x1 filters. This reduces the number of parameters in the model while still allowing it to learn complex representations. Additionally, SqueezeNet uses a technique called "fire modules" which combine a 1x1 convolutional layer with a mix of 1x1 and 3x3 convolutional layers to capture both local and global features.

SqueezeNet achieves a high degree of compression by using two main techniques:

Reducing the number of filters in convolutional layers by combining them with a technique called "channel squeezing." Channel squeezing involves using 1x1 convolutions to reduce the number of channels (or filters) in the input tensor.
Combining multiple layers into a single layer by using 3x3 convolutional filters that operate on a larger receptive field. This reduces the number of parameters required to learn the same features and also provides a computational advantage.
Overall, SqueezeNet achieves a good balance between model size and accuracy, making it well-suited for applications where computational resources are limited, such as mobile or embedded devices.

![SqueezeNet](./pics/squeezeNet.PNG)
![SqueezeNet_2](./pics/squeezeNet_description.PNG)


### Yolov8s
Ultralytics YOLOv8, developed by Ultralytics, is a cutting-edge, state-of-the-art (SOTA) model that builds upon the success of previous YOLO versions and introduces new features and improvements to further boost performance and flexibility. YOLOv8 is designed to be fast, accurate, and easy to use, making it an excellent choice for a wide range of object detection, image segmentation and image classification tasks.
![Yolov8s](./pics/yolov8.PNG)
![Yolov8s_2](./pics/yolov8_description.PNG)



### MobileNet
MobileNet is a family of deep learning architectures for image classification and other computer vision tasks. It was developed by researchers at Google in 2017 as a way to enable efficient deep learning on mobile and embedded devices with limited computational resources.

The main idea behind MobileNet is to use depthwise separable convolutions, which separate the standard convolution operation into two separate operations: a depthwise convolution that applies a single filter to each input channel, followed by a pointwise convolution that applies a 1x1 convolution to combine the outputs of the depthwise convolution. This approach reduces the number of computations required by the convolutional layer while still allowing the model to learn complex representations.

MobileNet also uses other techniques to reduce the number of parameters and computations, such as:

Width Multiplier: This technique reduces the number of channels in each layer by a factor called the width multiplier, which reduces the overall number of parameters.
Resolution Multiplier: This technique reduces the resolution of the input image by a factor called the resolution multiplier, which further reduces the number of computations.
MobileNet has several variations, including MobileNet V1, MobileNet V2, and MobileNet V3, each with different improvements and modifications. Overall, MobileNet is a popular architecture for mobile and embedded deep learning applications, as it achieves good accuracy while requiring fewer parameters and computations than other deep learning architectures.
![MobileNet](./pics/mobileNetV2.PNG)
![MobileNet_2](./pics/mobileNetV2_description.PNG)


## [Video demo](./movie/demo.mp4)