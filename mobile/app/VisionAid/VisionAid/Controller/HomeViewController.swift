//
//  HomeViewController.swift
//  VisionAid
//
//  Created by PAJAK Alexandre on 09/04/2023.
//

import Foundation
import UIKit
import Network

class HomeViewController: UIViewController {
    static let TCPclient = TCPClient.init(host: "54.37.153.179", port: 2005)
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var startButton: UIButton!
    let settings = getSettings()
    override func viewDidLoad() {
        super.viewDidLoad()
        // init struct
        var defaultSettings = Preference(uri: "None", streamName: "None", Width: 720, Height: 1280, videobitrate: 160, audiobitrate: 30, frameRate: 30, userID: "None")
        // if no settings ask to user
        if (settings.count == 0) {
            startButton.isEnabled = false
            label.text = "You must provide your userID. \n go to https://alpagam.com/VisionAid/register \n next enter your userID in settings."
        }
        // run connection
        HomeViewController.TCPclient.start()
        // send connection request with user id
        let data = "*connect*" + settings[0].userID!
        // get in response the rtmp new server url
        defaultSettings.uri = HomeViewController.TCPclient.getResponse(data: data.data(using: .utf8)!)
        defaultSettings.streamName = settings[0].userID!
        // save in coreData db
        saveSettings(settings: defaultSettings)
        label.text = "Welcome ! your stream address is:" + defaultSettings.uri + "/" + defaultSettings.streamName
    }
}
