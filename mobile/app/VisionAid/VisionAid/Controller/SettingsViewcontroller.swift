//
//  SettingsViewcontroller.swift
//  VisionAid
//
//  Created by PAJAK Alexandre on 10/04/2023.
//

import Foundation
import UIKit

class SettingsViewController: UIViewController {
    
    
    @IBOutlet weak var frameRate: UISegmentedControl!
    @IBOutlet weak var videoLabel: UILabel!
    @IBOutlet weak var audioSlider: UISlider!
    @IBOutlet weak var audioLabel: UILabel!
    @IBOutlet weak var key: UITextField!
    @IBOutlet weak var uri: UITextField!
    @IBOutlet weak var userID: UITextField!
    @IBOutlet weak var videoSlider: UISlider!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // set current setings in view
        
        // get settings from coreData
        let settings = getSettings()[0]
        // set framerate segmented bar
        switch settings.frameRate {
        case 15:
            frameRate.selectedSegmentIndex = 0
        case 30:
            frameRate.selectedSegmentIndex = 1
        case 60:
            frameRate.selectedSegmentIndex = 2
        default:
            break
        }
        // set video slider
        videoSlider.value = settings.videobitrate / 1000
        videoLabel.text = "video \(Int(videoSlider.value))/kbps"
        // set audio slider
        audioSlider.value = settings.audiobitrate / 1000
        audioLabel.text = "audio \(Int(audioSlider.value))/kbps"
        key.text = settings.streamName
        uri.text = settings.uri
        userID.text = settings.userID
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        var frameRateValue: Int
        switch frameRate.selectedSegmentIndex {
        case 0:
            frameRateValue = 15
        case 1:
            frameRateValue = 30
        case 2:
            frameRateValue = 60
        default:
            frameRateValue = 30
        }
        let newSettings = Preference(uri: uri.text!, streamName: key.text!, Width: 720, Height: 1280, videobitrate: videoSlider.value * 1000, audiobitrate: audioSlider.value * 1000, frameRate: frameRateValue, userID: userID.text!)
        
    }
    
    @IBAction func key(_ sender: Any) {
    }
    @IBAction func uri(_ sender: Any) {
    }
    @IBAction func userID(_ sender: Any) {
    }
    @IBAction func frameRate(_ sender: Any) {
    }
    @IBAction func audioSlider(_ sender: Any) {
    }
    @IBAction func videoslider(_ sender: Any) {
    }
    
    
    
}
