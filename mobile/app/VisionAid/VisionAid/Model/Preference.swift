//
//  Preference.swift
//  VisionAid
//
//  Created by PAJAK Alexandre on 08/04/2023.
//

struct Preference {
    var uri: String
    var streamName: String
    var Width: Int
    var Height: Int
    var videobitrate: Float
    var audiobitrate: Float
    var frameRate: Int
    var userID: String
}
