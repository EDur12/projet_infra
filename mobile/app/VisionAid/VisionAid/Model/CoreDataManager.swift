//
//  CoreDataManager.swift
//  VisionAid
//
//  Created by PAJAK Alexandre on 09/04/2023.
//

import Foundation
import CoreData

func saveSettings(settings: Preference) {
    // init instance
    let preference = Settings(context: CoreDataStack.sharedInstance.viewContext)
    // make change
    preference.frameRate = Int16(settings.frameRate)
    preference.userID = settings.userID
    preference.uri = settings.uri
    preference.videobitrate = Float(settings.videobitrate)
    preference.audiobitrate = Float(settings.audiobitrate)
    preference.height = Int16(settings.Height)
    preference.width = Int16(settings.Width)
    preference.streamName = settings.streamName
    // save
    do {
        try CoreDataStack.sharedInstance.viewContext.save()
        print("Settings Saved")
    } catch {
        print("CoreData unable to save settings for:  \(preference.userID ?? "NoUser") infos.")
    }
    //check
    printCoreDataUsers()
}

func getSettings()-> Array<Settings>{
    var settingsList: [Settings] = []
    let request: NSFetchRequest<Settings> = Settings.fetchRequest()
    guard let settings = try? CoreDataStack.sharedInstance.viewContext.fetch(request) else {return settingsList}
    for setting in settings {
        settingsList.append(setting)
        }
    return settingsList
}

func deleteAllSettings(){
    let request: NSFetchRequest<Settings> = Settings.fetchRequest()
    guard let settingsList = try? CoreDataStack.sharedInstance.viewContext.fetch(request) else {return}
    for settings in settingsList {
        CoreDataStack.sharedInstance.viewContext.delete(settings)
        // save
        do {
            try CoreDataStack.sharedInstance.viewContext.save()
            print("settings removed")
        } catch {
            print("CoreData unable to remove")
        }
        
    }
}


func printCoreDataUsers(){
    let request: NSFetchRequest<Settings> = Settings.fetchRequest()
    guard let settingsList = try? CoreDataStack.sharedInstance.viewContext.fetch(request) else {return}
    var settingsTab: [String] = []
    for settings in settingsList {
        settingsTab.append(settings.userID ?? "ND")
        }
    print(settingsTab)
    }

    

