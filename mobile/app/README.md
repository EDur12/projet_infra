# VisionAId App
VisionAId stream video from camera and detect objects and places in real time. It also uses Core ML to classify images using a pre-trained model.

## Requirements

- Xcode 9.0 or later
- iOS 11.0 or later
- Haishinkit 1.4.4
- CoreML
- Vision

## Installation

- Clone the repo
- Open `VisionAI.xcodeproj` in Xcode
- Build and run the app on a device

## Models
for add models to the app, you can download models from the following link:
https://github.com/john-rocky/CoreML-Models

and add them to the project at the following path:
[path](VisionAid/VisionAid/Model/models)

the app will automatically detect the models and add them to the list.
(caution: the app will not work if the model is not in the correct format ```.mlmodel``` or if the model is too big)

## Usage
- You need to register into our website to get an user id and a token to use the app
- You can register here: https://alpagam.com/visionaid/register
- Select a model from the list
- Tap the stream button to start the stream
- Tap the stop button to stop the stream
- Tap the settings button to change the stream settings
- Tap the info button to get more information about the model

## demo

[here](./movie/demo.mp4)