import cv2
import numpy as np
from ffpyplayer.player import MediaPlayer
import time

player = MediaPlayer("rtmp://54.37.153.179:1935/live/stream_1")

while True:
    frame, val = player.get_frame()
    if val == 'eof': 
        break # this is the difference
    if frame is not None:
        image, pts = frame
        w, h = image.get_size()

        # convert to array width, height
        img = np.asarray(image.to_bytearray()[0]).reshape(h,w,3)

        # convert RGB to BGR because `cv2` need it to display it
        img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)

        time.sleep(val)
        cv2.imshow('video', img)

        if cv2.waitKey(1) & 0xff == ord('q'):
            break

cv2.destroyAllWindows()
player.close_player()