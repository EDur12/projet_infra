# mac webcam ffmpeg stream to rtmp server at: rtmp://54.37.153.179:1935/live/stream
ffmpeg \
        -f avfoundation \
        -framerate 30 \
        -video_size 640x480 \
        -i "0:none" \
        -vcodec libx264 \
        -preset ultrafast \
        -tune zerolatency \
        -pix_fmt yuv422p \
        -f flv "rtmp://54.37.153.179:1935/live/stream_2"

# for stream download video to rtmp server
#ffmpeg -re -i videoTest.mkv -c:v copy -c:a aac -ar 44100 -ac 1 -f flv rtmp://127.0.0.1/live/stream

# test ffplay
#ffplay -i "0:none" -fflags nobuffer "rtmp://54.37.153.179:1935/live/stream"